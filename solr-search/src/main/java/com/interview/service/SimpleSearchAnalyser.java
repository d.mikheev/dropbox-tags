package com.interview.service;

import com.interview.model.TagsFileModel;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service for analysing search results
 *
 * @author d.mikheev on 31.03.19
 */
@Service
public class SimpleSearchAnalyser implements SearchAnalyser {

    private static Long MAX_FILE_SIZE = 500000000l;

    @Override
    /**
     * @inheritDoc
     */
    public boolean downloadResultsNotValid(List<TagsFileModel> tagsFiles) {
        Long size = 0l;
        for(TagsFileModel tagsFileModel:tagsFiles) {
            size += tagsFileModel.getSize();
        }
        if (size>MAX_FILE_SIZE)
            return true;
        return false;
    }
}
