package com.interview.service;

import com.interview.model.ConnectionType;
import com.interview.model.TagsFileModel;

import java.io.FileNotFoundException;
import java.util.List;

/**
 * Solr DAO service
 *
 * @author d.mikheev on 24.03.19
 */
public interface TagsManager {

    /**
     * Create file entity in solr repository
     *
     * @param filePath Path to file
     * @param fileSize File size in bytes
     * @param tags     List of tags
     */
    void addFileTags(String filePath, Long fileSize, List<String> tags);

    /**
     * Try to add tags in solr file entity
     *
     * @param filePath Path to file
     * @param tags     Added tags
     * @return All file tags
     * @throws FileNotFoundException
     */
    List<String> addTags(String filePath, List<String> tags) throws FileNotFoundException;

    /**
     * Delete all file entities from solr repository
     */
    void removeAll();

    /**
     * Delete file entity from solr repository
     *
     * @param filePath Path to file
     */
    void removeFile(String filePath);

    /**
     * Try to remove tags from solr file entity
     *
     * @param path Path to file
     * @param tags Removing tags
     * @return List on remaining tags
     * @throws FileNotFoundException
     */
    List<String> removeTags(String path, List<String> tags) throws FileNotFoundException;

    /**
     * Pageble search by tags
     *
     * @param connectionType Mode of connection OR or AND. OR=0, AND=1
     * @param page           Zero-based page index
     * @param pageSize       The size of the page to be returned
     * @param searchTags     Searching tags
     * @return Solr file entities models
     */
    List<TagsFileModel> findFilesByTagsPageble(ConnectionType connectionType, int page, int pageSize, List<String> searchTags);

    /**
     * Search by tags
     *
     * @param connectionType Mode of connection OR or AND. OR=0, AND=1
     * @param searchTags     Searching tags
     * @return Solr file entities models
     */
    List<TagsFileModel> findFilesByTags(ConnectionType connectionType, List<String> searchTags);

}
