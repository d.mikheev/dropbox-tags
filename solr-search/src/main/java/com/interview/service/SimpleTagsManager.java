package com.interview.service;

import com.interview.model.ConnectionType;
import com.interview.model.TagsFile;
import com.interview.model.TagsFileModel;
import com.interview.repository.TagsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
/**
 * Simple Solr DAO implementation
 */
public class SimpleTagsManager implements TagsManager {

    @Autowired
    private TagsRepository tagsRepository;

    /**
     * Prepare special search string for solr
     *
     * @param type       Mode of connection OR or AND. OR=0, AND=1
     * @param searchTags search tags
     * @return Search term string
     */
    private static String prepareSearchTerm(ConnectionType type, List<String> searchTags) {
        return searchTags.stream()
                .map(e -> "tags:" + e + "")
                .collect(Collectors.joining(" " + type.name() + " "));
    }

    @Override
    /**
     * @inheritDoc
     */
    public void addFileTags(String filePath, Long fileSize, List<String> addTags) {
        tagsRepository.findById(filePath)
                .ifPresentOrElse(tagsFile -> tagsRepository.save(populateTags(tagsFile, addTags)),
                        () -> tagsRepository.save(new TagsFile(filePath, fileSize, addTags)));
    }

    @Override
    /**
     * @inheritDoc
     */
    public List<String> addTags(String filePath, List<String> addTags) throws FileNotFoundException {
        TagsFile tagsFile = tagsRepository.findById(filePath).orElseThrow(() -> new FileNotFoundException(filePath));
        tagsFile = populateTags(tagsFile, addTags);
        tagsRepository.save(tagsFile);
        return tagsFile.getTags();
    }

    @Override
    /**
     * @inheritDoc
     */
    public List<String> removeTags(String filePath, List<String> removeTags) throws FileNotFoundException {
        TagsFile fileTags = Optional.ofNullable(tagsRepository.findById(filePath))
                .map(e -> removeTags(e.get(), removeTags))
                .orElseThrow(() -> new FileNotFoundException(filePath));
        tagsRepository.save(fileTags);
        return fileTags.getTags();
    }

    @Override
    /**
     * @inheritDoc
     */
    public void removeFile(String filePath) {
        tagsRepository.deleteById(filePath);
    }

    @Override
    /**
     * @inheritDoc
     */
    public void removeAll() {
        tagsRepository.deleteAll();
    }

    @Override
    /**
     * @inheritDoc
     */
    public List<TagsFileModel> findFilesByTagsPageble(ConnectionType connectionType, int page, int pageSize, List<String> searchTags) {
        String searchTerm = prepareSearchTerm(connectionType, searchTags);
        System.out.println(searchTerm);
        Page<TagsFile> tagsFilesPage = tagsRepository.findByPagebleCustomQuery(searchTerm, PageRequest.of(page, pageSize));
        List<TagsFile> tagsFiles = tagsFilesPage.getContent();
        return tagsFiles.stream().map(e -> new TagsFileModel(e.getId(), e.getTags(), e.getSize())).collect(Collectors.toList());
    }

    @Override
    /**
     * @inheritDoc
     */
    public List<TagsFileModel> findFilesByTags(ConnectionType connectionType, List<String> searchTags) {
        String searchTerm = prepareSearchTerm(connectionType, searchTags);
        List<TagsFile> tagsFiles = tagsRepository.findByCustomQuery(searchTerm);
        return tagsFiles.stream().map(e -> new TagsFileModel(e.getId(), e.getTags(), e.getSize())).collect(Collectors.toList());
    }

    /**
     * Remove tags from tagsFile
     *
     * @param tagsFile   original solr file entity
     * @param removeTags tags to remove
     * @return tagsFile without removeTags
     */
    public TagsFile removeTags(TagsFile tagsFile, List<String> removeTags) {
        for (String removeTag : removeTags) {
            if (tagsFile.getTags().contains(removeTag))
                tagsFile.getTags().remove(removeTag);
        }
        return tagsFile;
    }

    /**
     * Add tags to tagsFile
     *
     * @param tagsFile original solr file entity
     * @param newTags  tags to add
     * @returnt agsFile with newTags
     */
    private TagsFile populateTags(TagsFile tagsFile, List<String> newTags) {
        for (String newTag : newTags) {
            if (tagsFile.getTags().contains(newTag))
                continue;
            tagsFile.getTags().add(newTag);
        }
        return tagsFile;
    }
}
