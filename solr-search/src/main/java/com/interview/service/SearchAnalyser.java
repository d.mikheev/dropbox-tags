package com.interview.service;

import com.interview.model.TagsFileModel;

import java.util.List;

/**
 * Service for analysing search results
 *
 * @author d.mikheev on 31.03.19
 */
public interface SearchAnalyser {

    /**
     * Size chexk of th downloading files
     *
     * @param tagsFiles Solr file entity model
     * @return
     */
    public boolean downloadResultsNotValid(List<TagsFileModel> tagsFiles);
}
