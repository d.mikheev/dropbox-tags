package com.interview.model;

import java.util.List;

/**
 * Solr document entity model
 *
 * @author d.mikheev on 31.03.19
 */
public class TagsFileModel {

    private final String id;
    private final List<String> tags;
    private final Long size;

    public TagsFileModel(String id, List<String> tags, Long size) {
        this.id = "/" + id; // FIXME: temporary workaround
        this.tags = tags;
        this.size = size;
    }

    public String getId() {
        return id;
    }

    public List<String> getTags() {
        return tags;
    }

    public Long getSize() {
        return size;
    }
}
