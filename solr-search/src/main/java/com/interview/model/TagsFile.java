package com.interview.model;

import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.annotation.Id;
import org.springframework.data.solr.core.mapping.SolrDocument;

import java.util.List;

/**
 * Solr document entity
 *
 * @author d.mikheev on 24.03.19
 */
@SolrDocument(collection = "files")
public class TagsFile {

    @Id
    @Field
    private String id;
    @Field
    private List<String> tags;
    @Field
    private Long size;


    public TagsFile() {
    }

    public TagsFile(String id, Long fileSize, List<String> tags) {
        this.id = id;
        this.tags = tags;
        this.size = fileSize;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }
}
