package com.interview.model;

import java.security.InvalidParameterException;

/**
 * Connection type dictionary
 *
 * @author d.mikheev on 30.03.19
 */
public enum ConnectionType {

    OR(0),
    AND(1);

    private final int type;

    ConnectionType(int connectionType){
        this.type = connectionType;
    }

    public int getType() {
        return type;
    }

    public static ConnectionType valueOf(int type) {
        if (type==0)
            return ConnectionType.OR;
        if (type==1)
            return ConnectionType.AND;
        throw new IllegalArgumentException(String.format("Invalid connection type %s",type));
    }


}
