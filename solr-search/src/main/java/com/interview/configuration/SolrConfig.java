package com.interview.configuration;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.data.solr.repository.config.EnableSolrRepositories;

/**
 * @author d.mikheev on 24.03.19
 */
@Configuration
@EnableSolrRepositories(
        basePackages = "com.interview")
@ComponentScan
@PropertySource("classpath:solr.properties")
public class SolrConfig {

    private String solrEndpoint;

    public SolrConfig(@Value( "${app.solr-endpoint}" ) String solrEndpoint){
        this.solrEndpoint = solrEndpoint;
    };

    @Bean
    public SolrClient solrClient() {
        return new HttpSolrClient.Builder(solrEndpoint).build();
    }

    @Bean
    public SolrTemplate solrTemplate(SolrClient client) {
        return new SolrTemplate(client);
    }
}
