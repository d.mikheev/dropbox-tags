package com.interview.repository;

import com.interview.model.TagsFile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.solr.repository.Query;
import org.springframework.data.solr.repository.SolrCrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Solr CRUD repository
 *
 * @author d.mikheev on 24.03.19
 */
@Repository
public interface TagsRepository extends SolrCrudRepository<TagsFile, String> {

    @Query("?0")
    Page<TagsFile> findByPagebleCustomQuery(String searchTerm, Pageable pageable);


    @Query("?0")
    List<TagsFile> findByCustomQuery(String searchTerm);

}
