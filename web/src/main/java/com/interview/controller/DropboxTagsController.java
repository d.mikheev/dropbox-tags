package com.interview.controller;

import com.dropbox.core.DbxException;
import com.interview.Core;
import com.interview.model.*;
import com.interview.service.SearchAnalyser;
import com.interview.service.TagsManager;
import com.interview.util.EscapeUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.commons.math3.util.Pair;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;
import java.util.stream.Collectors;

@org.springframework.web.bind.annotation.RestController
@RequestMapping("/api/")
@Api(value = "tags API", description = "Tags managing API")
public class DropboxTagsController {

    @Autowired
    private Core core;
    @Autowired
    private TagsManager tagsManager;
    @Autowired
    private SearchAnalyser searchAnalyser;

    @ApiOperation(value = "Upload file with tags", response = FileImportInfoResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "File successfully uploaded"),
    })
    @RequestMapping(value = "/uploadFileWithTags", method = RequestMethod.POST)
    @ResponseBody
    public FileImportInfoResponse addFileWithTags(@RequestBody FileWithTagsRequest request) throws IOException, DbxException {
        Pair<String, Long> pathAndSize = core.addFile(request.getFilePath());
        tagsManager.addFileTags(EscapeUtil.removeFirstSlash(request.getFilePath()), pathAndSize.getValue(), request.getTags());


        return new FileImportInfoResponse(pathAndSize);
    }

    @ApiOperation(value = "Add tags to already uploaded file", response = TagsInfoResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Tags successfully added"),
            @ApiResponse(code = 404, message = "File not found by this path")
    })
    @RequestMapping(value = "/addTags", method = RequestMethod.POST)
    @ResponseBody
    public TagsInfoResponse addTags(@RequestBody FileWithTagsRequest request) {
        try {
            List<String> tagsAfterAdd = tagsManager.addTags(EscapeUtil.removeFirstSlash(request.getFilePath()), request.getTags());
            return new TagsInfoResponse(tagsAfterAdd.stream().collect(Collectors.joining(" ")));
        } catch (FileNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("File not found by path=%s", request.getFilePath()), e);
        }
    }

    @ApiOperation(value = "Remove file from dropbox", response = ResponseEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully removed"),
    })
    @RequestMapping(value = "/removeFile", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity removeFile(@RequestBody FilePathRequest request) throws DbxException {
        core.removeFile(request.getFilePath());
        tagsManager.removeFile(request.getFilePath());
        return new ResponseEntity(HttpStatus.METHOD_NOT_ALLOWED);
    }

    @ApiOperation(value = "Remove tags in already uploaded file", response = TagsInfoResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Tags successfully removed"),
            @ApiResponse(code = 404, message = "File not found by this path")
    })
    @RequestMapping(value = "/removeTags", method = RequestMethod.POST)
    @ResponseBody
    public TagsInfoResponse removeTags(@RequestBody FileWithTagsRequest request) {
        try {
            List<String> tagsAfterRemove = tagsManager.removeTags(EscapeUtil.removeFirstSlash(request.getFilePath()), request.getTags());
            return new TagsInfoResponse(tagsAfterRemove.stream().collect(Collectors.joining(" ")));
        } catch (FileNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("File not found by path=%s", request.getFilePath()), e);
        }
    }

    @ApiOperation(value = "Search files by tags", response = TagsInfoResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Search results")
    })
    @RequestMapping(value = "/searchByTags", method = RequestMethod.POST)
    @ResponseBody
    public SearchResultResponse searchByTags(@RequestBody FilesSearchRequest request) {
        List<TagsFileModel> tagsFileModels = tagsManager.findFilesByTagsPageble(ConnectionType.valueOf(request.getConnectivityType()), request.getPageNum(), request.getPageSize(), request.getTags());
        return new SearchResultResponse(tagsFileModels);
    }

    @ApiOperation(value = "Get zip multi-compressed file")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "File  successfully downloaded"),
            @ApiResponse(code = 404, message = "File not found by this tags and connectivity type"),
            @ApiResponse(code = 409, message = "Files common size more then acceptable")
    })
    @RequestMapping(value = "/downloadByTags", method = RequestMethod.POST)
    public void downloadByTags(@RequestBody FilesRequest request, HttpServletResponse response) throws IOException, DbxException {
        List<TagsFileModel> tagsFileModels = tagsManager.findFilesByTags(ConnectionType.valueOf(request.getConnectivityType()), request.getTags());
        if (tagsFileModels.isEmpty())
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Files not found by tags %s and connectivity type %s", request.getTags().toString(), ConnectionType.valueOf(request.getConnectivityType()).toString()));
        if (searchAnalyser.downloadResultsNotValid(tagsFileModels))
            throw new ResponseStatusException(HttpStatus.CONFLICT, String.format("Files common size more then %s expected", "500 MB"));

        String zipFileName = core.dowloadZip(tagsFileModels);
        File file = new File(zipFileName);
        response.setContentType("application/zip");
        response.setHeader("Content-disposition", "attachment; filename=" + file.getName());
        OutputStream out = response.getOutputStream();
        FileInputStream in = new FileInputStream(file);
        IOUtils.copy(in, out);
        out.close();
        in.close();
        file.delete();
    }

    @ApiOperation(value = "Remove all data from solr")
    @RequestMapping(value = "/clear", method = RequestMethod.GET)
    public void clear() {
        tagsManager.removeAll();
    }

}
