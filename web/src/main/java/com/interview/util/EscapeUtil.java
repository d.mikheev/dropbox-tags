package com.interview.util;

/**
 * @author d.mikheev on 30.03.19
 */
public class EscapeUtil {

    public static String removeFirstSlash(String string){
        if (string.startsWith("/"))
            return string.substring(1,string.length());
        return string;
    }
}
