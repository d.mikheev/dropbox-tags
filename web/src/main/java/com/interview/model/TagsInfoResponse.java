package com.interview.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author d.mikheev on 30.03.19
 */
@ApiModel(value="TagsInfoResponse", description="Model of TagsInfoResponse")
public class TagsInfoResponse {

    private final String updatedTags;

    public TagsInfoResponse(String updatedTags){
        this.updatedTags = updatedTags;
    }

    @ApiModelProperty(value = "String of updated tags", example = "ocean birds sunset")
    public String getUpdatedTags() {
        return updatedTags;
    }
}
