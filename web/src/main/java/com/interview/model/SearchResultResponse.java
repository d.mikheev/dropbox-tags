package com.interview.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 * @author d.mikheev on 30.03.19
 */
@ApiModel(value="SearchResultResponse", description="Model of SearchResultResponse")
public class SearchResultResponse {

    private List<TagsFileModel> tagsFiles;

    public SearchResultResponse(List<TagsFileModel> tagsFiles) {
        this.tagsFiles = tagsFiles;
    }

    @ApiModelProperty(value = "File tags params")
    public List<TagsFileModel> getTagsFiles() {
        return tagsFiles;
    }
}
