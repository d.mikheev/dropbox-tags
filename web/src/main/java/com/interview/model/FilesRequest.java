package com.interview.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 * @author d.mikheev on 31.03.19
 */
@ApiModel(value="FilesRequest", description="Model of FilesRequest")
public class FilesRequest {

    private Integer connectivityType;
    private List<String> tags;


    public FilesRequest(){}

    public FilesRequest(Integer connectivityType, List<String> tags) {
        this.connectivityType = connectivityType;
        this.tags = tags;
    }

    @ApiModelProperty(value = "Mode of connection OR or AND. OR=0, AND=1", example = "0")
    public Integer getConnectivityType() {
        return connectivityType;
    }

    @ApiModelProperty(value = "Tags adding with the file", example = "[\"ocean\", \"girls\", \"sunset\"]")
    public List<String> getTags() {
        return tags;
    }

    public void setConnectivityType(Integer connectivityType) {
        this.connectivityType = connectivityType;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }
}
