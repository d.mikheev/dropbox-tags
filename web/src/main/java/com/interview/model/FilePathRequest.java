package com.interview.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author d.mikheev on 01.04.19
 */
@ApiModel(value="FilePathRequest", description="Model of FilePathRequest")
public class FilePathRequest {
    private String filePath;

    public FilePathRequest(){}

    public FilePathRequest(String filePath) {
        this.filePath = filePath;
    }
    @ApiModelProperty(value = "File path string to file", example = "/home/user/Desktop/file.txt")
    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
}
