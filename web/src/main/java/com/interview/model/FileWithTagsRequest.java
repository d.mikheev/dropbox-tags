package com.interview.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.annotations.*;

import java.io.Serializable;
import java.util.List;

/**
 * @author d.mikheev on 30.03.19
 */
@JsonSerialize
@ApiModel(value="FileWithTagsRequest", description="Model of FileWithTagsRequest")
public class FileWithTagsRequest implements Serializable {
    private String filePath;
    private List<String> tags;

    public FileWithTagsRequest(){
    }

    public FileWithTagsRequest(String filePath, List<String> tags) {
        this.filePath = filePath;
        this.tags = tags;
    }
    @ApiModelProperty(value = "File path string to file", example = "/home/user/Desktop/file.txt")
    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    @ApiModelProperty(value = "Tags adding with the file", example = "[\"ocean\", \"birds\", \"sunset\"]")
    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }
}
