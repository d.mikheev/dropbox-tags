package com.interview.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.math3.util.Pair;

/**
 * @author d.mikheev on 30.03.19
 */
public class FileImportInfoResponse extends Pair {

    public FileImportInfoResponse(Pair pair) {
        super(pair.getKey(), pair.getValue());
    }

    @Override
    @JsonProperty("path")
    public Object getKey() {
        return super.getKey();
    }

    @Override
    @JsonProperty("size")
    public Object getValue() {
        return super.getValue();
    }
}
