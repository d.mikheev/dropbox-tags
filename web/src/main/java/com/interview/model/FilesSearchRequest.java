package com.interview.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * @author d.mikheev on 30.03.19
 */
@JsonSerialize
@ApiModel(value="FilesSearchRequest", description="Model of FilesSearchRequest")
public class FilesSearchRequest implements Serializable {

    private Integer pageNum;
    private Integer pageSize;
    private Integer connectivityType;
    private List<String> tags;

    public FilesSearchRequest(){}

    public FilesSearchRequest(Integer pageNum, Integer pageSize, Integer connectivityType, List<String> tags) {
        this.pageNum = pageNum;
        this.pageSize = pageSize;
        this.connectivityType = connectivityType;
        this.tags = tags;
    }

    @ApiModelProperty(value = "Zero-based page index", example = "0")
    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    @ApiModelProperty(value = "The size of the page to be returned", example = "25")
    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    @ApiModelProperty(value = "Mode of connection OR or AND. OR=0, AND=1", example = "0")
    public Integer getConnectivityType() {
        return connectivityType;
    }

    public void setConnectivityType(Integer connectivityType) {
        this.connectivityType = connectivityType;
    }
    @ApiModelProperty(value = "Tags adding with the file", example = "[\"ocean\", \"girls\", \"sunset\"]")
    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }
}
