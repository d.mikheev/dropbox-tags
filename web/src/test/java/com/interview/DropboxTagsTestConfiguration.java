package com.interview;

import javafx.application.Application;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author d.mikheev on 31.03.19
 */
@Configuration
@Import(Application.class)
public class DropboxTagsTestConfiguration {

//    private static final String CORE_NAME = "userIndex";
//    //Override the client to start embedded instance of solr
//    @Bean
//    @Primary
//    public SolrClient solrClient() {
//        CoreContainer coreContainer = CoreContainer.createAndLoad(Paths.get("src/test/resources/com/interview/solr/").toAbsolutePath());
//        EmbeddedSolrServer newServer = new EmbeddedSolrServer(coreContainer, CORE_NAME);
//        return newServer;
//    }
//
//    @Bean
//    @Primary
//    public TagsManager tagsManager() {
//        return new SimpleTagsManager();
//    }
}
