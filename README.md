# Dropbox-Tags-App
### Description
I was using Spring Boot, Spring Data, Dropbox SDK to implement the assigment. Unfortunately I did't have enough time to finish integration tests, prepare docker image and make external configuration.  

#### API documentation
The API documentation in swagger format can be found under the following URL: http://localhost:8080/swagger-ui.html
Or in case you run the app on different host, you should replace the host name with the one where app is actually running.

#### Modules
- app - Spring Boot settings and docker building properties
- core - Module for integration with Dropbox SDK
- solr-search - Module for integration with Solr
- web - Module contains REST API and all web components

#### Before run
- Change Solr address in com.interview.configuration.SolrConfig BASE_SOLR_URL. Current value of BASE_SOLR_URL = "http://localhost:8983/solr"
- Create index "files" in Solr using command:
```bash
sudo su - solr -c "/opt/solr/bin/solr create -c files -n data_driven_schema_configs"
```
- Change Dropbox security token in com.interview.Core ACCESS_TOKEN or use current.
- Change Max Zip File size in com.interview.service.SimpleSearchAnalyzer MAX_FILE_SIZE. Current value of MAX_FILE_SIZE is 500 000 000 bytes.

### Running instructions.
1. Download or clone the project.
2. Change to the root project directory.
3. Build the project using gradle wrapper:
```bash
./gradlew :app:bootJar
```
On windows use
```bash
gradlew.bat :app:bootJar
```
4. Change to the <project root>/app/build/libs
5. Run the jar:
```bash
  java -jar dropbox-tags-app-0.1.0.jar
  ```
If something isn't working or you have any questions, feel free to contact me.