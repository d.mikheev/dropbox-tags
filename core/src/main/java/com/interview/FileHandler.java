package com.interview;

import com.dropbox.core.DbxException;
import com.interview.model.TagsFileModel;
import org.apache.commons.math3.util.Pair;

import java.io.IOException;
import java.util.List;

/**
 * Operations with Dropbox SDK
 *
 * @author d.mikheev on 24.03.19
 */
public interface FileHandler {

    /**
     * Upload file to dropbox account
     *
     * @param path Path to file
     * @return Pair. Key is path_to_file and Value is file_size
     * @throws DbxException
     * @throws IOException
     */
    Pair<String, Long> addFile(String path) throws DbxException, IOException;

    /**
     * Remove file from dropbox account
     *
     * @param path Path to file
     * @throws DbxException
     */
    void removeFile(String path) throws DbxException;

    /**
     * Download files from dropbox account by Solr file entities models
     *
     * @param tagsFiles Solr file entities models
     * @return multi zip file filename
     * @throws DbxException
     * @throws IOException
     */
    String dowloadZip(List<TagsFileModel> tagsFiles) throws DbxException, IOException;

}

