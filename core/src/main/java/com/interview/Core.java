package com.interview;

import com.dropbox.core.DbxDownloader;
import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.DownloadZipResult;
import com.dropbox.core.v2.files.FileMetadata;
import com.dropbox.core.v2.users.FullAccount;
import com.interview.model.TagsFileModel;
import com.interview.util.PathUtil;
import org.apache.commons.math3.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author d.mikheev on 24.03.19
 */
@Service
public class Core implements FileHandler{

    private static final String MULTI_ZIP_FILE_NAME = "multiCompressed.zip";

    @Autowired
    private DbxClientV2 client;

//    public Core(){
//        DbxRequestConfig config = DbxRequestConfig.newBuilder("dropbox/d.mikheev").build();
//        this.client = new DbxClientV2(config, ACCESS_TOKEN);
//    }

    public String getAccountName(){
        FullAccount account = null;
        try {
            account = client.users().getCurrentAccount();
        } catch (DbxException e) {
            e.printStackTrace();
        }
        return account.getName().getGivenName();
    }

    public void search() throws DbxException {
        DbxDownloader<DownloadZipResult> downloader = client.files().downloadZip("/lol");
        DownloadZipResult zipResult = downloader.getResult();
    }

    @Override
    public Pair<String,Long> addFile(String path) throws DbxException, IOException {
        InputStream in = new FileInputStream(path);
        try {
            FileMetadata fileMetadata = client.files().upload(path).uploadAndFinish(in);
            return new Pair<>(fileMetadata.getPathLower(),fileMetadata.getSize());
        }
        finally {
            in.close();
        }
    }

    private String downloadFile(String path) throws DbxException, IOException {
        String downloadFileName = PathUtil.getFileNameFromPath(path);
        OutputStream downloadFile = new FileOutputStream(downloadFileName);
        try {
            FileMetadata metadata = client.files().downloadBuilder(path)
                    .download(downloadFile);
            return downloadFileName;
        }
        finally {
            downloadFile.close();
        }
    }

    public String dowloadZip(List<TagsFileModel> tagsFiles) throws DbxException, IOException {
        FileOutputStream fos = new FileOutputStream(MULTI_ZIP_FILE_NAME);
        ZipOutputStream zipOut = new ZipOutputStream(fos);
        for(TagsFileModel tagsFile:tagsFiles){
            String downloadFileName = downloadFile(tagsFile.getId());
            File fileToZip = new File(downloadFileName);
            FileInputStream fis = new FileInputStream(fileToZip);
            ZipEntry zipEntry = new ZipEntry(fileToZip.getName());
            zipOut.putNextEntry(zipEntry);

            byte[] bytes = new byte[1024];
            int length;
            while((length = fis.read(bytes)) >= 0) {
                zipOut.write(bytes, 0, length);
            }
            fis.close();
            fileToZip.delete();
        }
        zipOut.close();
        fos.close();
        return MULTI_ZIP_FILE_NAME;
    }

    @Override
    public void removeFile(String path) throws DbxException {
        client.files().deleteV2(path);
    }
}
