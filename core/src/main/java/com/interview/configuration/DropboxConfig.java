package com.interview.configuration;

import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * @author d.mikheev on 14.04.19
 */
@Configuration
@PropertySource("classpath:dropbox.properties")
public class DropboxConfig {

    private String clientIdentifier;
    private String accessToken;
    private String multiZipFileName;

    public DropboxConfig(@Value("${app.client-identifier}") String clientIdentifier, @Value("${app.access-token}")
            String accessToken, @Value("${app.multi-zip-file-name}") String multiZipFileName) {
        this.clientIdentifier = clientIdentifier;
        this.accessToken = accessToken;
        this.multiZipFileName = multiZipFileName;
    }

    @Bean
    public DbxClientV2 createDbxClient() {
        DbxRequestConfig config = DbxRequestConfig.newBuilder(clientIdentifier).build();
        return new DbxClientV2(config, accessToken);
    }
}
