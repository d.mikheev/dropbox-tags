package com.interview.util;

/**
 * @author d.mikheev on 31.03.19
 */
public class PathUtil {

    /**
     * Receives fileName from path string
     *
     * @param path Path to file
     * @return
     */
    public static String getFileNameFromPath(String path){
        if (path.contains("/")){
            String[] treeNodes = path.split("/");
            return treeNodes[treeNodes.length-1];
        }
        return path;
    }
}
