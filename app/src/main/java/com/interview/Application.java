package com.interview;

import com.interview.configuration.AppProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@SpringBootApplication()
@EntityScan("com.interview")
@EnableSwagger2
//@EnableConfigurationProperties(AppProperties.class)
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public Docket newsApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("dropbox-tags-api")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.interview"))
                .paths(PathSelectors.ant("/api/*"))
                .build()
                .apiInfo(apiInfo());


    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Rest API that extends the Dropbox API functionality by introducing tags")
                .description("The API allows you ...")
                .termsOfServiceUrl("www.example.com/terms-of-service")
                .license("Apache License Version 2.0")
                .licenseUrl("https://github.com/LICENSE")
                .version("2.0")
                .build();
    }
}