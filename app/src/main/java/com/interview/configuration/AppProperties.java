package com.interview.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;

/**
 * @author d.mikheev on 13.04.19
 */
@ConfigurationProperties(prefix = "app")
@Data
public class AppProperties {
    @NotNull
    private String solrEndpoint;

}
